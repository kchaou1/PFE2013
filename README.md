<b>PFE 2013

<b>Sujet: Réalisation d'un site web de vente en ligne

<b>Description : Le projet consiste à la conception et réalisation d’un site web de vente en ligne de matériels informatique au profil de la société MTD groupe. Le site web conçu a permis de rendre service d’une part, au client en lui permettant de choisir les produits et de passer à la suite à un service de payement sécurisé (Paypal). D’autre part, ce site permet à un administrateur (Back office) de suivre la gestion des produits commandés et des clients.

<b>Rapport : https://github.com/aniskchaou/Realisation_un_site_web_de_vente_en_ligne_PFE2013/blob/master/rapport_PFE2013.pdf

<b>Siège: MTDGroup

<b>Date: 15/02/2013 --> 15/05/2013

<b>Technologies utilisés : php,framework cakephp,bootstrap,JQuery,JQueryUI
